﻿using _2EF_Core_Local.Context;
using _2EF_Core_Local.Models;
using Microsoft.EntityFrameworkCore;

namespace _2EF_Core_Local.Entity
{
    public class ProductEntity
    {
        private readonly AppDBContext _db;

        public ProductEntity(AppDBContext db)
        {
            _db = db;
        }

        public async Task<List<Product>> GetProduct()
        {
            List<Product> wt = new List<Product>();
            try
            {
                wt = await _db.Products.ToListAsync();
            }
            catch
            {
                throw;
            }
            return wt;
        }

        public async Task<int> SaveProduct(Product input)
        {
            int wt = 0;
            try
            {
                input.Id = Guid.NewGuid();
                _db.Products.Add(input);
                await _db.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
            return wt;
        }

    }
}
