﻿using System.Collections.Generic;
using _2EF_Core_Local.Models;
using Microsoft.EntityFrameworkCore;

namespace _2EF_Core_Local.Context
{
    public class AppDBContext : DbContext
    {
        protected readonly IConfiguration Configuration;
        public AppDBContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to sqlite database
            options.UseSqlite(Configuration.GetConnectionString("DefaultConnection"));
        }


        public DbSet<Product> Products { get; set; }
    }
}
