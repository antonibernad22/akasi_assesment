using _2EF_Core_Local.Interface;
using _2EF_Core_Local.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace _2EF_Core_Local.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IProduct _iproduct;

        public HomeController(ILogger<HomeController> logger, IProduct iproduct)
        {
            _logger = logger;
            _iproduct = iproduct;
        }

        public async Task<IActionResult> Index()
        {
            var Products = await _iproduct.GetProduct();
            return View(Products);
        }
        [HttpPost]
        public async Task<IActionResult> Index(Product input)
        {
            await _iproduct.SaveProduct(input);
            var Products = await _iproduct.GetProduct();
            return View(Products);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
