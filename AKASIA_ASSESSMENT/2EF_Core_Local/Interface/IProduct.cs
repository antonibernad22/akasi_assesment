﻿using _2EF_Core_Local.Models;

namespace _2EF_Core_Local.Interface
{
    public interface IProduct
    {
        Task<List<Product>> GetProduct();
        Task<int> SaveProduct(Product input);
    }
}
