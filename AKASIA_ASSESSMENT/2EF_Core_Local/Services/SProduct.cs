﻿using _2EF_Core_Local.Context;
using _2EF_Core_Local.Entity;
using _2EF_Core_Local.Interface;
using _2EF_Core_Local.Models;

namespace _2EF_Core_Local.Services
{
    public class SProduct : IProduct
    {
        private readonly ProductEntity _ProductEntity;
        private readonly AppDBContext _appDB;
        public SProduct(AppDBContext appDB)
        {
            _appDB = appDB;
            _ProductEntity = new ProductEntity(_appDB);
        }
        public async Task<List<Product>> GetProduct()
        {
            List<Product> wt = new List<Product>();
            try
            {
                wt = await _ProductEntity.GetProduct();
            }
            catch
            {
                throw;
            }
            return wt;
        }

        public async Task<int> SaveProduct(Product input)
        {
            int wt = 0;
            try
            {
                await _ProductEntity.SaveProduct(input);
            }
            catch
            {
                throw;
            }
            return wt;
        }
    }
}
