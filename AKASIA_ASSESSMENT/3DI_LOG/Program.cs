﻿using _3DI_LOG.Interface;
using _3DI_LOG.Services;
using Microsoft.Extensions.DependencyInjection;

class Program
{
    static void Main(string[] args)
    {
        var serviceProvider = new ServiceCollection()
            //untuk consoleLog
            //.AddSingleton<ILogger, ConsoleLogger>() 
            //untuk FileLog
            .AddSingleton<ILogger>(provider => new FileLogger(@"C:\AKASIALog\log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt"))
            .BuildServiceProvider();

        var Logger = serviceProvider.GetRequiredService<ILogger>();
        Logger.Log("Logger TEST.");


        Console.ReadLine();
    }
}