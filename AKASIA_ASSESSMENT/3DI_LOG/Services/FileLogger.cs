﻿using _3DI_LOG.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3DI_LOG.Services
{
    public class FileLogger : ILogger
    {
        private readonly string filePath;

        public FileLogger(string filePath)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(filePath);
            file.Directory.Create();
            this.filePath = filePath;
        }

        public void Log(string message)
        {
            File.AppendAllText(filePath, $"File Log {DateTime.Now}: {message}\n");
        }
    }
}
